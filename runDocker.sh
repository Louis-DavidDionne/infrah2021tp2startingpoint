docker stop tp2ldd
docker container rm tp2ldd
docker image rm tp2ldd_image
docker volume rm tp2ldd_vol

docker volume create --name tp2ldd_vol --opt device=$PWD --opt o=bind --opt type=none
docker build -t tp2ldd_image -f ./project/docker/Dockerfile .

docker run -p 5555:5555 --mount source=tp2ldd_vol,target=/mnt/app/ --name tp2ldd tp2ldd_image #-d tp2ldd_image