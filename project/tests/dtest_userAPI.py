import unittest
import requests
import json
from codeAPI import userAPI


IP = "127.0.0.1"
PORT = "5555"
URL = "http://" + IP + ":" + PORT + "/"

class BasicTests(unittest.TestCase):

	def test_hello(self):
		response = requests.get(URL)
		self.assertEqual(200, response.status_code)

		obj = json.loads(response.content.decode('utf-8'))
		self.assertEqual('2000', obj['code'])
		self.assertIn('msg', obj)

	def test_delUser(self):
#TODO
		print("Test disabled. You need to code the getusers route before you can successfully run this test.")
		#response = requests.get(URL + "adduser?username=Roy")
		#self.assertEqual(200, response.status_code)

		#response = requests.get(URL + "getusers?" + API_KEY)
		#obj = json.loads(response.content.decode('utf-8'))
		#self.assertIn('Roy', obj['msg']['NewUsers'])

		#response = requests.get(URL + "deluser?username=Roy")
		#self.assertEqual(200, response.status_code)
		#obj = json.loads(response.content.decode('utf-8'))
		#self.assertEqual('2000', obj['code'])

		#response = requests.get(URL + "getusers")
		#obj = json.loads(response.content.decode('utf-8'))
		#self.assertNotIn('Roy', obj['msg']['NewUsers'])

	def test_delUserNoParam(self):
		response = requests.get(URL + "deluser")
		self.assertEqual(400, response.status_code)
		obj = json.loads(response.content.decode('utf-8'))
		self.assertEqual('1000', obj['code'])

	def test_delUserNonExisting(self):
		response = requests.get(URL + "deluser?username=Brodeur")
		self.assertEqual(400, response.status_code)
		obj = json.loads(response.content.decode('utf-8'))
		self.assertEqual('1002', obj['code'])

	def test_delInitialUser(self):
#TODO
		print("Test disabled. You need to complete the flow for route /deluser (i.e., the methode oswrap.readFileByLine()) before you can successfully run this test.")
		#response = requests.get(URL + "deluser?username=root")
		#self.assertEqual(400, response.status_code)
		#obj = json.loads(response.content.decode('utf-8'))
		#self.assertEqual('1003', obj['code'])


if __name__ == '__main__':
	unittest.main()
