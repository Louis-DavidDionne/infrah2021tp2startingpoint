import subprocess

DEFAULT_PASSWORD = ''

def runCommandToAddUser(username):
	subprocess.run(['useradd', '-p', DEFAULT_PASSWORD, username])

def runCommandToRemoveUser(username):
	subprocess.run(['userdel', username])

def readFileByLine(filename):
	content = []
	file = open(filename, "r")
	for line in file:
		content.append(file.readline())
	file.close()
	return content
